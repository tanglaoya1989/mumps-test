  program test_mumps
  ! this function is used to test mumps for complex matrix
  USE ZMUMPS_STRUC_DEF
  implicit none
  TYPE (ZMUMPS_STRUC)::mumps_par
  integer::i,n,nz,sym
  integer,pointer::irow(:),jcol(:)
  real*8::t1,t2,res,zresidual_coo
  complex*16,pointer::a(:),x(:),b(:),r(:)
  open(1,file='mat',form='unformatted')
  read(1)n,nz
  allocate(irow(nz),jcol(nz),a(nz),x(n),b(n),r(n))
  do i=1,nz
  read(1)irow(i),jcol(i),t1,t2
  a(i)=dcmplx(t1,t2)
  enddo
  do i=1,n
  read(1)t1,t2
  x(i)=dcmplx(t1,t2)
  b(i)=x(i)
  enddo
  close(1)
  sym=2
  mumps_par%SYM = sym
  mumps_par%JOB = -1
  CALL ZMUMPS(mumps_par)
  mumps_par%JOB = 6
  mumps_par%icntl(10) = 5   ! iterative refinement
  mumps_par%icntl(11) = 1   ! the METIS package is used
  mumps_par%N=n
  mumps_par%NZ=nz
  mumps_par%IRN=>irow
  mumps_par%JCN=>jcol
  mumps_par%A=>a
  mumps_par%RHS=>x 
  CALL ZMUMPS(mumps_par)
  res = zresidual_coo(n, nz, irow, jcol, a, x, b, r,sym)
  deallocate(irow,jcol,a,x,b,r)
  mumps_par%JOB = -2
  CALL ZMUMPS(mumps_par)
  end program
  
	real*8 function zresidual_coo(n, nz, irow, jcol, val, x, b, r,sym)result (out)
	implicit none
	integer::n, i,nz, irow(nz), jcol(nz),sym
	complex*16::val(nz), x(n), b(n), r(n)
	CHARACTER*1   uplo
	real*8, external:: dznrm2
	if(sym==1.or.sym==2)then
	  do i=1,n
	    if(irow(i)/=jcol(i))then
	      if(irow(i)>jcol(i))then
	        uplo='l'
	      else
	        uplo='u'
	      endif
	      exit
	    endif
	  enddo  
	  call mkl_zcoosymv(uplo, n, val, irow, jcol, nz, x, r)
	else
	  call mkl_zcoogemv('n', n, val, irow, jcol, nz, x, r)
	endif
	do i=1,n
	  r(i) = r(i)-b(i)
	enddo
	out = dznrm2(n,b,1)
	out=dznrm2(n,r,1)/out 
	end function 
  
