[0] MPI startup(): I_MPI_MIC_PROXY_PATH environment variable is not supported.
[0] MPI startup(): To check the list of supported variables, use the impi_info utility or refer to https://software.intel.com/en-us/mpi-library/documentation/get-started.

Entering ZMUMPS 5.4.0 with JOB, N, NNZ =   6     2329813       20182013
      executing #MPI =      1, without OMP

 =================================================
 MUMPS compiled with option -Dmetis
 =================================================
L D L^T Solver for general symmetric matrices
Type of parallelism: Working host

 ****** ANALYSIS STEP ********

 Processing a graph of size:   2329813
 Average density of rows/columns =   15
 Ordering based on METIS
 ELAPSED TIME SPENT IN METIS reordering  =     40.9710

Leaving analysis phase with  ...
 INFOG(1)                                       =               0
 INFOG(2)                                       =               0
 -- (20) Number of entries in factors (estim.)  =       552458787
 --  (3) Real space for factors    (estimated)  =       628796106
 --  (4) Integer space for factors (estimated)  =        31020426
 --  (5) Maximum frontal size      (estimated)  =            3509
 --  (6) Number of nodes in the tree            =          242061
 -- (32) Type of analysis effectively used      =               1
 --  (7) Ordering option effectively used       =               5
 ICNTL (6) Maximum transversal option           =               0
 ICNTL (7) Pivot order option                   =               7
 ICNTL(14) Percentage of memory relaxation      =              20
 ICNTL(18) Distributed input matrix (on if >0)  =               0
 Number of level 2 nodes                        =               0
 Number of split nodes                          =               0
 RINFOG(1) Operations during elimination (estim)= 4.012D+11

 MEMORY ESTIMATIONS ... 
 Estimations with standard Full-Rank (FR) factorization:
    Total space in MBytes, IC factorization      (INFOG(17)):       12758
    Total space in MBytes,  OOC factorization    (INFOG(27)):        1018

 Elapsed time in analysis driver=      50.3480


****** FACTORIZATION STEP ********

 GLOBAL STATISTICS PRIOR NUMERICAL FACTORIZATION ...
 Number of working processes                =               1
 ICNTL(22) Out-of-core option               =               0
 ICNTL(35) BLR activation (eff. choice)     =               0
 ICNTL(14) Memory relaxation                =              20
 INFOG(3) Real space for factors (estimated)=       628796106
 INFOG(4) Integer space for factors (estim.)=        31020426
 Maximum frontal size (estimated)           =            3509
 Number of nodes in the tree                =          242061
 Memory allowed (MB -- 0: N/A )             =               0
 Memory provided by user, sum of LWK_USER   =               0
 Effective threshold for pivoting, CNTL(1)  =      0.1000D-01
 Max difference from 1 after scaling the entries for ONE-NORM (option 7/8)   = 0.16D+00
 Effective size of S     (based on INFO(39))=            755467546

 Elapsed time to reformat/distribute matrix =      2.7740

 ** Memory allocated, total in Mbytes           (INFOG(19)):       12758
 ** Memory effectively used, total in Mbytes    (INFOG(22)):       10744

 Elapsed time for factorization             =    120.1300

Leaving factorization with ...
 RINFOG(2)  Operations in node assembly     = 9.436D+08
 ------(3)  Operations in node elimination  = 4.012D+11
 ICNTL (8)  Scaling effectively used        =               7
 INFOG (9)  Real space for factors          =       628796106
 INFOG(10)  Integer space for factors       =        31020426
 INFOG(11)  Maximum front size              =            3509
 INFOG(29)  Number of entries in factors    =       552458787
 INFOG(13)  Number of delayed pivots        =               0
 Number of 2x2 pivots in type 1 nodes       =               0
 Number of 2x2 pivots in type 2 nodes       =               0
 INFOG(14)  Number of memory compress       =               0

 Elapsed time in factorization driver=     124.9180


 ****** SOLVE & CHECK STEP ********

 GLOBAL STATISTICS PRIOR SOLVE PHASE ...........
 Number of right-hand-sides                    =           1
 Blocking factor for multiple rhs              =           1
 ICNTL (9)                                     =           1
  --- (10)                                     =           0
  --- (11)                                     =           1
  --- (20)                                     =           0
  --- (21)                                     =           0
  --- (30)                                     =           0
  --- (35)                                     =           0

 ERROR ANALYSIS

 RESIDUAL IS ............ (MAX-NORM)        = 1.63D-18
                       .. (2-NORM)          = 2.49D-17
 RINFOG(4):NORM OF input  Matrix  (MAX-NORM)= 1.07D+03
 RINFOG(5):NORM OF Computed SOLUT (MAX-NORM)= 3.49D-01
 RINFOG(6):SCALED RESIDUAL ...... (MAX-NORM)= 4.38D-21
 RINFOG(7):COMPONENTWISE SCALED RESIDUAL(W1)= 1.96D-15
 ------(8):---------------------------- (W2)= 4.35D-22
 ------(9):Upper bound ERROR ...............= 4.24D-11
 -----(10):CONDITION NUMBER (1) ............= 2.16D+04
 -----(11):CONDITION NUMBER (2) ............= 2.53D+05
 ** Space in MBYTES used for solve                        :     12804
 
 Leaving solve with ...
 Time to build/scatter RHS        =       0.131000
 Time in solution step (fwd/bwd)  =       3.155000
  .. Time in forward (fwd) step   =          1.515000
  .. Time in backward (bwd) step  =          1.639000
 Time to gather solution(cent.sol)=       0.053000
 Time to copy/scale dist. solution=       0.000000

 Elapsed time in solve driver=      36.9170
 res (|Ax-b|/|b|)=  1.529483211902143E-014

Entering ZMUMPS 5.4.0 with JOB =  -2
      executing #MPI =      1, without OMP
